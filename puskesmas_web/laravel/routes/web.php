<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\DokterController;
use App\Http\Controllers\LayananController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\GaleriController;
use App\Http\Controllers\PengaduanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 Route Tampilan User
*/

Route::get('/', 'PengaduanController@create')->name('pengaduan.create');
Route::post('/tambah', 'PengaduanController@store')->name('pengaduan.store');

Route::get('/profil', function () {
    $data['active-page'] = 'profil';
    return view('profil', ['data' => $data]);
});

Route::get('/profil/visimisi', function () {
    $data['active-page'] = 'profil';
    return view('profilvisimisi', ['data' => $data]);
});

Route::get('/profil/denah', function () {
    $data['active-page'] = 'profil';
    return view('profildenah', ['data' => $data]);
});

Route::get('/layanan', function () {
    $layanan = new LayananController();
    $data['data-layanan'] = $layanan->show();
    $data['active-page'] = 'layanan';
    return view('layanan', ['data' => $data]);
});

Route::get('/informasi', function () {
    $berita = new BeritaController();
    $data['data-berita'] = $berita->show();
    $artikel = new ArtikelController();
    $data['data-artikel'] = $artikel->show();

    // Aside data
    $data['berita-aside'] = $berita->show('1');
    $data['artikel-aside'] = $artikel->show('1');

    $data['active-page'] = 'informasi';
    return view('informasi', ['data' => $data]);
})->name('berita.showDetail');

Route::get('/berita', function () {
    $berita = new BeritaController();
    $data['data-berita'] = $berita->show();
    $artikel = new ArtikelController();
    $data['data-artikel'] = $artikel->show();

    // Aside data
    $data['berita-aside'] = $berita->show('1');
    $data['artikel-aside'] = $artikel->show('1');

    $data['active-page'] = 'informasi';
    return view('berita', ['data' => $data]);
});

Route::get('/berita/{berita}', 'BeritaController@showDetail')->name('berita.showDetail');

Route::get('/artikel/{artikel}', 'ArtikelController@showDetail')->name('artikel.showDetail');

Route::get('/artikel', function () {
    $artikel = new ArtikelController();
    $data['data-artikel'] = $artikel->show();
    $berita = new BeritaController();
    $data['data-berita'] = $berita->show();

    // Aside data
    $data['berita-aside'] = $berita->show('1');
    $data['artikel-aside'] = $artikel->show('1');

    $data['active-page'] = 'informasi';
    return view('artikel', ['data' => $data]);
});

Route::get('/dokter', function () {
    $dokter = new DokterController();
    $data['data-dokter'] = $dokter->show();
    $data['active-page'] = 'dokter';
    return view('dokter', ['data' => $data]);
});

Route::get('/kontak', function () {
    $data['active-page'] = 'kontak';
    return view('kontak', ['data' => $data]);
});

Route::get('/login', function () {
    return view('login');
});

/*
 Route Login Admin
*/
Route::get('/login', 'AdminController@index')->name('login.index');
Route::get('/logout', 'AdminController@logout')->name('login.logout');
Route::post('/login', 'AdminController@process')->name('login.process');

/*
 Route Admin Layout
*/
Route::get('/admin/dashboard', function () {
    $data['active'] = 'admin-dashboard';

    // count data
    $layanan    = new LayananController();
    $dokter     = new DokterController();
    $berita     = new BeritaController();
    $artikel    = new ArtikelController();
    $galeri     = new GaleriController();
    $pengaduan  = new PengaduanController();

    $data['layanan-count']      = $layanan->show()->count();
    $data['dokter-count']       = $dokter->show()->count();
    $data['berita-count']       = $berita->show()->count();
    $data['artikel-count']      = $artikel->show()->count();
    $data['galeri-count']       = $galeri->show()->count();
    $data['pengaduan-count']    = $pengaduan->show()->count();

    return view('admin-layout.admin-dashboard', ['data' => $data]);
})->middleware('login_auth');


/*
 Route Admin CRUD
*/
Route::get('/admin/layanan', 'LayananController@index')->name('admin.layanan')->middleware('login_auth');
Route::get('/admin/layanan/input', 'LayananController@create')->name('admin.layanan.create')->middleware('login_auth');
Route::post('/admin/layanan', 'LayananController@store')->name('admin.layanan.store')->middleware('login_auth');
Route::get('/admin/layanan/edit/{id}', 'LayananController@edit')->name('admin.layanan.edit')->middleware('login_auth');
Route::patch('/admin/layanan/{id}', 'LayananController@update')->name('admin.layanan.update')->middleware('login_auth');
Route::delete('/admin/layanan/{id}', 'LayananController@delete')->name('admin.layanan.delete')->middleware('login_auth');

Route::get('/admin/berita', 'BeritaController@index')->name('admin.berita')->middleware('login_auth');
Route::get('/admin/berita/input', 'BeritaController@create')->name('admin.berita.create')->middleware('login_auth');
Route::post('/admin/berita', 'BeritaController@store')->name('admin.berita.store')->middleware('login_auth');
Route::get('/admin/berita/edit/{id}', 'BeritaController@edit')->name('admin.berita.edit')->middleware('login_auth');
Route::patch('/admin/berita/{id}', 'BeritaController@update')->name('admin.berita.update')->middleware('login_auth');
Route::delete('/admin/berita/{id}', 'BeritaController@delete')->name('admin.berita.delete')->middleware('login_auth');

Route::get('/admin/artikel', 'ArtikelController@index')->name('admin.artikel')->middleware('login_auth');
Route::get('/admin/artikel/input', 'ArtikelController@create')->name('admin.artikel.create')->middleware('login_auth');
Route::post('/admin/artikel', 'ArtikelController@store')->name('admin.artikel.store')->middleware('login_auth');
Route::get('/admin/artikel/edit/{id}', 'ArtikelController@edit')->name('admin.artikel.edit')->middleware('login_auth');
Route::patch('/admin/artikel/{id}', 'ArtikelController@update')->name('admin.artikel.update')->middleware('login_auth');
Route::delete('/admin/artikel/{id}', 'ArtikelController@delete')->name('admin.artikel.delete')->middleware('login_auth');

Route::get('/admin/galeri', 'GaleriController@index')->name('admin.galeri')->middleware('login_auth');
Route::get('/admin/galeri/input', 'GaleriController@create')->name('admin.galeri.create')->middleware('login_auth');
Route::post('/admin/galeri', 'GaleriController@store')->name('admin.galeri.store')->middleware('login_auth');
Route::get('/admin/galeri/edit/{id}', 'GaleriController@edit')->name('admin.galeri.edit')->middleware('login_auth');
Route::patch('/admin/galeri/{id}', 'GaleriController@update')->name('admin.galeri.update')->middleware('login_auth');
Route::delete('/admin/galeri/{id}', 'GaleriController@delete')->name('admin.galeri.delete')->middleware('login_auth');

Route::get('/admin/pengaduan', 'PengaduanController@index')->name('admin.pengaduan')->middleware('login_auth');

Route::get('/admin/dokter', 'DokterController@index')->name('admin.dokter')->middleware('login_auth');
Route::get('/admin/dokter/input', 'DokterController@create')->name('admin.dokter.create')->middleware('login_auth');
Route::post('/admin/dokter', 'DokterController@store')->name('admin.dokter.store')->middleware('login_auth');
Route::get('/admin/dokter/edit/{id}', 'DokterController@edit')->name('admin.dokter.edit')->middleware('login_auth');
Route::patch('/admin/dokter/{id}', 'DokterController@update')->name('admin.dokter.update')->middleware('login_auth');
Route::delete('/admin/dokter/{id}', 'DokterController@delete')->name('admin.dokter.delete')->middleware('login_auth');