<?php

namespace App\Http\Controllers;

use App\Dokter;
use Illuminate\Http\Request;
use File;

class DokterController extends Controller
{
    public function index()
    {
        $data['active'] = 'admin-dokter';
        $data['data-dokter'] = Dokter::all();

        return view('admin-layout.dokter.admin-dokter', ['data' => $data]);
    }

    public function show()
    {
        return Dokter::all();
    }

    public function create(Request $request, $id = null)
    {
        $webData['active'] = 'admin-dokter';
        $webData['mode'] = 'Add';
        $webData['data-dokter'] = null;

        return view('admin-layout.dokter.admin-dokter-create', ['data' => $webData]);
    }

    public function store(Request $request)
    {
        $data['active'] = 'admin-dokter';
        $validateData = $request ->validate([
            'img_doctors'      => 'required|file|image|max:2000',
            'name_doctors'   => '',
            'specialist_doctors'    => '',
            'schedule_doctors'     => '',
        ]);

        $doctors = new Dokter();
        
        if($request->hasFile('img_doctors'))
        {
            $extFile = $request->img_doctors->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->img_doctors->move('assets/images/dokter',$namaFile);
            $doctors->img_doctors = $path;
        }
        $doctors->name_doctors  = $validateData['name_doctors'];
        $doctors->specialist_doctors   = $validateData['specialist_doctors'];
        $doctors->schedule_doctors    = $validateData['schedule_doctors'];
        
        $doctors->save();
        
        $request->session()->flash('Pesan', 'Penambahan data berhasil');
        
        return redirect()->route('admin.dokter');
    }

    public function edit(Request $request, $id)
    {
        $data['active'] = 'admin-dokter';
        $data['mode']   = 'Edit';
        $data['data-dokter'] = Dokter::findOrFail($id);

        return view('admin-layout.dokter.admin-dokter-edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $data['active'] = 'admin-dokter';
        $validateData = $request ->validate([
            'img_doctors'      => 'required|file|image|max:2000',
            'name_doctors'   => '',
            'specialist_doctors'    => '',
            'schedule_doctors'     => '',
        ]);

        $doctors = Dokter::findOrFail($id);
        
        if($request->hasFile('img_doctors'))
        {
            $extFile = $request->img_doctors->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($doctors->image);
            $path = $request->img_doctors->move('assets/images/dokter',$namaFile);
            $doctors->img_doctors = $path;
        }

        $doctors->name_doctors  = $validateData['name_doctors'];
        $doctors->specialist_doctors   = $validateData['specialist_doctors'];
        $doctors->schedule_doctors    = $validateData['schedule_doctors'];
        
        $doctors->save();
        
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('admin.dokter',['dokter' => $doctors->id]);
    }

    public function delete(Request $request, $id)
    {
        $doctors = Dokter::findOrFail($id);
        $doctors->delete();
        $request->session()->flash('pesan','Hapus data berhasil');

        return redirect()->route('admin.dokter');
    }
}
