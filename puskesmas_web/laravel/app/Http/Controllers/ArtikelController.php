<?php

namespace App\Http\Controllers;

use App\Artikel;
use App\Http\Controllers\BeritaController;
use Illuminate\Http\Request;
use File;

class ArtikelController extends Controller
{
    public function index()
    {
        $data['active'] = 'admin-artikel';
        $data['data-artikel'] = Artikel::all();

        return view('admin-layout.artikel.admin-artikel', ['data' => $data]);
    }

    public function show($p = 'all')
    {
        $data['active-page'] = 'informasi';

        if ($p == '1') {
            return Artikel::orderBy('created_at')
            ->take(2)
            ->get();
        }

        return Artikel::all();
    }

    public function showDetail(Request $request, $id)
    {   
        $data['active-page'] = 'informasi';
        $data['artikel-id'] = Artikel::findOrFail($id);
        $data['artikel-aside'] = Artikel::orderBy('created_at')
        ->take(2)
        ->get();

        $berita = new BeritaController();
        $data['berita-aside'] = $berita->show('2');
        
        return view('detail-artikel', ['data' => $data]);
    }

    public function create(Request $request, $id = null)
    {
        $webData['active'] = 'admin-artikel';
        $webData['mode'] = 'Add';
        $webData['data-artikel'] = null;

        return view('admin-layout.artikel.admin-artikel-create', ['data' => $webData]);
    }

    public function store(Request $request)
    {
        $data['active'] = 'admin-artikel';
        $validateData = $request ->validate([
            'img_article'      => 'required|file|image|max:2000',
            'author_article'   => '',
            'title_article'    => '',
            'desc_article'     => '',
        ]);

        $article = new Artikel();
        
        if($request->hasFile('img_article'))
        {
            $extFile = $request->img_article->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->img_article->move('assets/images/artikel',$namaFile);
            $article->img_article = $path;
        }
        $article->author_article  = $validateData['author_article'];
        $article->title_article   = $validateData['title_article'];
        $article->desc_article    = $validateData['desc_article'];
        
        $article->save();
        
        $request->session()->flash('Pesan', 'Penambahan data berhasil');
        
        return redirect()->route('admin.artikel');
    }

    public function edit(Request $request, $id)
    {
        $data['active'] = 'admin-artikel';
        $data['mode']   = 'Edit';
        $data['data-artikel'] = Artikel::findOrFail($id);

        return view('admin-layout.artikel.admin-artikel-edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $data['active'] = 'admin-artikel';
        $validateData = $request ->validate([
            'img_article'      => 'required|file|image|max:2000',
            'author_article'   => '',
            'title_article'    => '',
            'desc_article'     => '',
        ]);

        $article = Artikel::findOrFail($id);
        
        if($request->hasFile('img_article'))
        {
            $extFile = $request->img_article->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($article->image);
            $path = $request->img_article->move('assets/images/artikel',$namaFile);
            $article->img_article = $path;
        }

        $article->author_article  = $validateData['author_article'];
        $article->title_article   = $validateData['title_article'];
        $article->desc_article    = $validateData['desc_article'];
        
        $article->save();
        
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('admin.artikel',['artikel' => $article->id]);
    }

    public function delete(Request $request, $id)
    {
        $article = Artikel::findOrFail($id);
        $article->delete();
        $request->session()->flash('pesan','Hapus data berhasil');

        return redirect()->route('admin.artikel');
    }
}
