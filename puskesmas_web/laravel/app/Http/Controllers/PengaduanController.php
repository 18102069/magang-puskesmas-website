<?php

namespace App\Http\Controllers;

use App\Pengaduan;
use App\Http\Controllers\DokterController;
use App\Http\Controllers\LayananController;
use App\Http\Controllers\GaleriController;
use Illuminate\Http\Request;

class PengaduanController extends Controller
{
    public function index()
    {
        $data['active'] = 'admin-pengaduan';
        $data['data-pengaduan'] = Pengaduan::all();
        
        return view('admin-layout.admin-pengaduan', ['data' => $data]);
    }

    public function show()
    {
        return Pengaduan::all();
    }
    
    public function create(Request $request, $id = null)
    {
        $webData['active-page'] = 'home';
        $webData['mode'] = 'Add';
        $webData['data-pengaduan'] = null;

        $webData['pengaduan-highlight'] = Pengaduan::orderBy('created_at')
        ->take(10)
        ->get();
        
        $dokter = new DokterController();
        $webData['data-dokter'] = $dokter->show();

        $layanan = new LayananController();
        $webData['data-layanan'] = $layanan->show();

        $galeri = new GaleriController();
        $webData['data-galeri'] = $galeri->show();
        
        return view('home', ['data' => $webData]);
    }
    
    public function store(Request $request)
    {
        $data['active-page'] = 'home';
        
        $validateData = $request ->validate([
            'name_user'     => '',
            'no_user'       => '',
            'date'          => '',
            'message_user'  => '',
        ]);
        
        $pengaduan = new Pengaduan();
        
        $pengaduan->name_user       = $validateData['name_user'];
        $pengaduan->no_user         = $validateData['no_user'];
        $pengaduan->date            = $validateData['date'];
        $pengaduan->message_user    = $validateData['message_user'];
        
        $pengaduan->save();
        
        $request->session()->flash('Pesan', 'Pengaduan Telah Terkirim!');
        
        return redirect()->route('pengaduan.create');
    }
}
