<?php

namespace App\Http\Controllers;

use App\Galeri;
use Illuminate\Http\Request;
use File;

class GaleriController extends Controller
{
    public function index()
    {
        $data['active'] = 'admin-galeri';
        $data['data-galeri'] = Galeri::all();

        return view('admin-layout.galeri.admin-galeri', ['data' => $data]);
    }

    public function show()
    {
        return Galeri::all();
    }

    public function create(Request $request, $id = null)
    {
        $webData['active'] = 'admin-galeri';
        $webData['mode'] = 'Add';
        $webData['data-galeri'] = null;

        return view('admin-layout.galeri.admin-galeri-create', ['data' => $webData]);
    }

    public function store(Request $request)
    {
        $data['active'] = 'admin-galeri';
        $validateData = $request ->validate([
            'img_galery'      => 'required|file|image|max:2000',
            'title_galery'    => '',
        ]);

        $galery = new Galeri();
        
        if($request->hasFile('img_galery'))
        {
            $extFile = $request->img_galery->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->img_galery->move('assets/images/galeri',$namaFile);
            $galery->img_galery = $path;
        }
        $galery->title_galery   = $validateData['title_galery'];
        
        $galery->save();
        
        $request->session()->flash('Pesan', 'Penambahan data berhasil');
        
        return redirect()->route('admin.galeri');
    }

    public function edit(Request $request, $id)
    {
        $data['active'] = 'admin-galeri';
        $data['mode']   = 'Edit';
        $data['data-galeri'] = Galeri::findOrFail($id);

        return view('admin-layout.galeri.admin-galeri-edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $data['active'] = 'admin-galeri';
        $validateData = $request ->validate([
            'img_galery'      => 'required|file|image|max:2000',
            'title_galery'    => '',
        ]);

        $galery = Galeri::findOrFail($id);
        
        if($request->hasFile('img_galery'))
        {
            $extFile = $request->img_galery->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($galery->image);
            $path = $request->img_galery->move('assets/images/galeri',$namaFile);
            $galery->img_galery = $path;
        }

        $galery->title_galery   = $validateData['title_galery'];
        
        $galery->save();
        
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('admin.galeri',['galeri' => $galery->id]);
    }

    public function delete(Request $request, $id)
    {
        $galery = Galeri::findOrFail($id);
        $galery->delete();
        $request->session()->flash('pesan','Hapus data berhasil');

        return redirect()->route('admin.galeri');
    }
}
