<?php

namespace App\Http\Controllers;

use App\Layanan;
use Illuminate\Http\Request;
use File;

class LayananController extends Controller
{
    public function index()
    {
        $data['active'] = 'admin-layanan';
        $data['data-layanan'] = Layanan::all();
        return view('admin-layout.layanan.admin-layanan', ['data' => $data]);
    }

    public function show()
    {
        return Layanan::all();
    }
    
    public function create(Request $request, $id = null)
    {
        $webData['active'] = 'admin-layanan';
        $webData['mode'] = 'Add';
        $webData['data-layanan'] = null;
        return view('admin-layout.layanan.admin-layanan-create', ['data' => $webData]);   
        
    }
    
    public function store(Request $request)
    {
        $data['active'] = 'admin-layanan';
        $validateData = $request ->validate([
            'img_layanan'       => 'required|file|image|max:2000',
            'name_layanan'      => '',
            'desc_layanan'      => '',
        ]);
        $layanan = new Layanan();
        
        if($request->hasFile('img_layanan'))
        {
            $extFile = $request->img_layanan->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->img_layanan->move('assets/images',$namaFile);
            $layanan->img_layanan = $path;
        }
        $layanan->name_layanan = $validateData['name_layanan'];
        $layanan->desc_layanan = $validateData['desc_layanan'];
        $layanan->save();
        
        $request->session()->flash('Pesan', 'Penambahan data berhasil');
        
        return redirect()->route('admin.layanan');
    }
    
    public function edit(Request $request, $id)
    {
        $data['active'] = 'admin-layanan';
        $data['mode']   = 'Edit';
        $data['data-layanan'] = Layanan::findOrFail($id);
        return view('admin-layout.layanan.admin-layanan-edit', ['data' => $data]);
    }
    
    public function update(Request $request, $id)
    {
        $data['active'] = 'admin-layanan';
        $validateData = $request ->validate([
            'img_layanan'       => 'required|file|image|max:2000',
            'name_layanan'      => '',
            'desc_layanan'      => '',
        ]);
        $layanan = Layanan::findOrFail($id);
        
        if($request->hasFile('img_layanan'))
        {
            $extFile = $request->img_layanan->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($layanan->image);
            $path = $request->img_layanan->move('assets/images',$namaFile);
            $layanan->img_layanan = $path;
        }
        $layanan->name_layanan = $validateData['name_layanan'];
        $layanan->desc_layanan = $validateData['desc_layanan'];
        $layanan->save();
        
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('admin.layanan',['layanan' => $layanan->id]);
    }
    
    public function delete(Request $request, $id)
    {
        $layanan = Layanan::findOrFail($id);
        $layanan->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('admin.layanan');
    }
}
