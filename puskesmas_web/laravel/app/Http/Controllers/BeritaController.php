<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Http\Controllers\ArtikelController;
use Illuminate\Http\Request;
use File;

class BeritaController extends Controller
{

    public function index()
    {
        $data['active'] = 'admin-berita';
        $data['data-berita'] = Berita::all();
        
        return view('admin-layout.berita.admin-berita', ['data' => $data]);
    }
    
    public function show($p = 'all')
    {
        $data['active-page'] = 'informasi';

        if ($p == '1') {
            return Berita::orderBy('created_at')
            ->take(2)
            ->get();
        }

        return Berita::all();
    }
    
    public function showDetail(Request $request, $id)
    {   
        $data['active-page'] = 'informasi';
        $data['berita-id'] = Berita::findOrFail($id);
        $data['berita-aside'] = Berita::orderBy('created_at')
        ->take(2)
        ->get();

        $artikel = new ArtikelController();
        $data['artikel-aside'] = $artikel->show('2');
        
        return view('detail-berita', ['data' => $data]);
    }
    
    public function create(Request $request, $id = null)
    {
        $webData['active'] = 'admin-berita';
        $webData['mode'] = 'Add';
        $webData['data-berita'] = null;
        
        return view('admin-layout.berita.admin-berita-create', ['data' => $webData]);
    }
    
    public function store(Request $request)
    {
        $data['active'] = 'admin-berita';
        $validateData = $request ->validate([
            'img_news'      => 'required|file|image|max:2000',
            'author_news'   => '',
            'title_news'    => '',
            'desc_news'     => '',
        ]);
        
        $news = new Berita();
        
        if($request->hasFile('img_news'))
        {
            $extFile = $request->img_news->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->img_news->move('assets/images/berita',$namaFile);
            $news->img_news = $path;
        }
        $news->author_news  = $validateData['author_news'];
        $news->title_news   = $validateData['title_news'];
        $news->desc_news    = $validateData['desc_news'];
        
        $news->save();
        
        $request->session()->flash('Pesan', 'Penambahan data berhasil');
        
        return redirect()->route('admin.berita');
    }
    
    public function edit(Request $request, $id)
    {
        $data['active'] = 'admin-berita';
        $data['mode']   = 'Edit';
        $data['data-berita'] = Berita::findOrFail($id);
        return view('admin-layout.berita.admin-berita-edit', ['data' => $data]);
    }
    
    public function update(Request $request, $id)
    {
        $data['active'] = 'admin-berita';
        $validateData = $request ->validate([
            'img_news'      => 'required|file|image|max:2000',
            'author_news'   => '',
            'title_news'    => '',
            'desc_news'     => '',
        ]);
        $news = Berita::findOrFail($id);
        
        if($request->hasFile('img_news'))
        {
            $extFile = $request->img_news->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($news->image);
            $path = $request->img_news->move('assets/images/berita',$namaFile);
            $news->img_news = $path;
        }
        
        $news->author_news  = $validateData['author_news'];
        $news->title_news   = $validateData['title_news'];
        $news->desc_news    = $validateData['desc_news'];
        
        $news->save();
        
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('admin.berita',['berita' => $news->id]);
    }
    
    public function delete(Request $request, $id)
    {
        $news = Berita::findOrFail($id);
        $news->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        
        return redirect()->route('admin.berita');
    }
}
