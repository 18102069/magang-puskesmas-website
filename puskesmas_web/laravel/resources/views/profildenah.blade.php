@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">

  <!-- ======= Profil/About Section ======= -->
  <section id="about" class="about">
    <div class="container">
      <div class="section-title" style="margin-top: 10%">
        <h2>Denah Puskesmas Kajen 1</h2>

        <img src="{{url('')}}/laravel/resources/assets/img/informasi/denah-puskesmas-kajen-1.jpg" alt="">
      </div>
    </div>
  </section>

</main><!-- End #main -->
@endsection

@section('chat')
@include('landing-page.chat')
@endsection

@section('footer')
@include('landing-page.footer')
@endsection