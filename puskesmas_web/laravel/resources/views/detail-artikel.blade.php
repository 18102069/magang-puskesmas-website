@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
<link href="{{url('')}}/laravel/resources/css/information-style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">
  
  <!-- ======= Services Section ======= -->
  <section id="information" class="information">
    <div class="container">
      
      <div class="section-title" style="margin-top: 10%">
        <h2>{{$data['artikel-id']->title_article}}</h2>
        
        <div class="container-info body-info" style="padding: 20px 0px">
          <div class="col-md-8">

            <div style="text-align: left">
              <p class="far fa-calendar-alt">
                <strong>
                  {{$data['artikel-id']->created_at}}
                </strong>
              </p>

              <p class="fas fa-user">
                Ditulis Oleh : 
                <strong>
                  {{$data['artikel-id']->author_article}}
                </strong>
              </p>
            </div>
            
            {{-- <ul class="list-inline font-weight-600 myriadpro">
              <i class="fa fa-calendar">
                {{$data['artikel-id']->created_at}}
              </i>
              
              <i class="fa fa-user">
                "Ditulis Oleh : "
                <span class="label label-default">
                  {{$data['artikel-id']->author_article}}
                </span>
              </i>
            </ul> --}}
            
            <div>
              <img src="{{url('')}}/{{$data['artikel-id']->img_article}}" alt="" style="align-items: center; width: 600px; margin-top: 20px;">
            </div>
            
            <div>
              <h3 style="text-align: center; margin-top: 20px;">
                <strong>
                  {{$data['artikel-id']->title_article}}
                </strong>
              </h3>
              <p style="font-size: medium; text-align: justify; margin-top: 20px;">
                {{$data['artikel-id']->desc_article}}
              </p>
            </div>
          </div>
          
          @include('landing-page.aside')
        </div>
      </div>
    </section>
    <!-- End information Section -->
    
  </main><!-- End #main -->
  @endsection
  
  @section('chat')
  @include('landing-page.chat')
  @endsection
  
  @section('footer')
  @include('landing-page.footer')
  @endsection