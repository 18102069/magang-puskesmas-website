@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">
  
  <!-- ======= Dokter Section ======= -->
  <section id="doctors" class="doctors">
    <div class="container">
      
      <div class="section-title" style="margin-top: 10%">
        <h2>Dokter</h2>
      </div>
      
      <div class="row">
        @foreach ($data['data-dokter'] as $dokter)

        <div class="col-lg-6">
          <div class="member d-flex align-items-start">

            <div class="pic"><img src="{{url('')}}/{{$dokter->img_doctors}}" class="img-fluid" alt=""></div>
            
            <div class="member-info">
              <h4>{{$dokter['name_doctors']}}</h4>
              <span>{{$dokter['specialist_doctors']}}</span>
              <p>Jadwal Dokter: </p>
              <p>{{$dokter['schedule_doctors']}}</p>
              
            </div>
          </div>
        </div>

        @endforeach
        
      </div>
      
    </div>
  </section>
  <!-- End Doctors Section -->
  
</main><!-- End #main -->
@endsection

@section('chat')
@include('landing-page.chat')
@endsection

@section('footer')
@include('landing-page.footer')
@endsection
