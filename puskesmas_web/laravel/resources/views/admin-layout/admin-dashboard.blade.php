@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Selamat Datang!
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('admin/dashboard')}}">
                    <i class="fa fa-dashboard">
                    </i> 
                    Dashboard
                </a>
            </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        
        <!-- Info boxes -->
        <div class="row">

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Layanan</span>
                        <span class="info-box-number">{{$data['layanan-count']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Dokter</span>
                        <span class="info-box-number">{{$data['dokter-count']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-gear-outline"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Berita</span>
                        <span class="info-box-number">{{$data['berita-count']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-gear-outline"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Artikel</span>
                        <span class="info-box-number">{{$data['artikel-count']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-purple"><i class="ion ion-ios-gear-outline"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Galeri</span>
                        <span class="info-box-number">{{$data['galeri-count']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-lime"><i class="ion ion-ios-gear-outline"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Penilaian Masyarakat</span>
                        <span class="info-box-number">{{$data['pengaduan-count']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('footer')
@include('admin-page.footer')
@endsection