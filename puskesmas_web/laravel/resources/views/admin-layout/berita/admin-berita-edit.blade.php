@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Berita
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/berita')}}">Berita</a></li>
            <li>Edit</li>
        </ol>
    </section>
    <!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        
                        <!-- /.card-header -->
                        <div class="box">
                            
                            @if ($data['mode'] == 'Edit')
                            <form action="{{ route('admin.berita.update', ['id' => $data['data-berita']['id']]) }}" method="POST" enctype="multipart/form-data">
                                @method('PATCH')
                                @elseif($data['mode'] == 'Add')
                                <form action="{{ route('admin.berita.store') }}" method="POST" enctype="multipart/form-data">
                                    @endif
                                    
                                    @csrf
                                    <div class="form-group">
                                        <label for="author_news">Nama Penulis</label>
                                        
                                        <input type="text" class="@error('author_news') is-invalid @enderror input-group" name="author_news" id="author_news" @if ($data['data-berita'] != null) value="{{$data['data-berita']['author_news']}}" @endif>
                                        
                                        @error('author_news')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="title_news">Judul Berita</label>
                                        @error('title_news')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <textarea class="form-control @error('title_news') is-invalid @enderror" name="title_news" id="title_news" cols="30" rows="10">@if ($data['data-berita'] != null) {{$data['data-berita']['title_news']}} @endif</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="desc_news">Deskripsi</label>
                                        @error('desc_news')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <textarea class="form-control @error('desc_news') is-invalid @enderror" name="desc_news" id="desc_news" cols="30" rows="10">@if ($data['data-berita'] != null) {{$data['data-berita']['desc_news']}} @endif</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="img_news">Gambar</label>
                                        
                                        @error('img_news')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                        
                                        <input type="file" class="@error('img_news') is-invalid @enderror" name="img_news" id="img_news"></input>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-primary btn-block">Edit</button>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </div>
    @endsection
    
    @section('footer')
    @include('admin-page.footer')
    @endsection