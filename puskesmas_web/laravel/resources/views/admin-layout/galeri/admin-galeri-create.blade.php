@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tambah Galeri
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/galeri')}}">galeri</a></li>
            <li>Tambah</li>
        </ol>
    </section>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <!-- /.card-header -->
                        <div class="box">

                            @if ($data['mode'] == 'Edit')
                                <form action="{{ route('admin.galeri.store') }}" method="POST" enctype="multipart/form-data">
                                @method('PATCH')
                            @elseif($data['mode'] == 'Add')
                                <form action="{{ route('admin.galeri.store') }}" method="POST" enctype="multipart/form-data">
                            @endif

                                @csrf
                                
                                <div class="form-group">
                                    <label for="title_galery">Judul Galeri</label>
                                    @error('title_galery')
                                    <div class="alert alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    <textarea class="form-control @error('title_galery') is-invalid @enderror" name="title_galery" id="title_galery" cols="30" rows="10">@if ($data['data-galeri'] != null) {{$data['data-galeri']['title_galery']}} @endif</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="img_galery">Gambar</label>
                                    
                                    @error('img_galery')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                    
                                    <input type="file" class="@error('img_galery') is-invalid @enderror" name="img_galery" id="img_galery"></input>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
</div>
@endsection
    
@section('footer')
@include('admin-page.footer')
@endsection