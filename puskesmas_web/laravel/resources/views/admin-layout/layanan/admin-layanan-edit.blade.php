@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Layanan
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/layanan')}}">Layanan</a></li>
            <li>Edit</li>
        </ol>
    </section>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <!-- /.card-header -->
                        <div class="box">

                            @if ($data['mode'] == 'Edit')
                                <form action="{{ route('admin.layanan.update', ['id' => $data['data-layanan']['id']]) }}" method="POST" enctype="multipart/form-data">
                                @method('PATCH')
                            @elseif($data['mode'] == 'Add')
                                <form action="{{ route('admin.layanan.store') }}" method="POST" enctype="multipart/form-data">
                            @endif

                                @csrf
                                <div class="form-group">
                                    <label for="name_layanan">Nama Layanan</label>
                                    
                                    <input type="text" class="@error('name_layanan') is-invalid @enderror input-group" name="name_layanan" id="name_layanan" @if ($data['data-layanan'] != null) value="{{$data['data-layanan']['name_layanan']}}" @endif>
                                    
                                    @error('name_layanan')
                                    <div class="alert alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <label for="desc_layanan">Deskripsi</label>
                                    @error('desc_layanan')
                                    <div class="alert alert-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    <textarea class="form-control @error('desc_layanan') is-invalid @enderror" name="desc_layanan" id="desc_layanan" cols="30" rows="10">@if ($data['data-layanan'] != null) {{$data['data-layanan']['desc_layanan']}} @endif</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="img_layanan">Gambar</label>
                                    
                                    @error('img_layanan')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                    
                                    <input type="file" class="@error('img_layanan') is-invalid @enderror" name="img_layanan" id="img_layanan"></input>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Edit</button>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
</div>
@endsection
    
@section('footer')
@include('admin-page.footer')
@endsection