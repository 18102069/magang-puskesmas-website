@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<!-- Isi File siswa.php-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Layanan Puskesmas Kajen 1
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin-dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin-layanan')}}">Layanan</a></li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                
                <!-- /.box -->
                
                <div class="box">
                    <div class="box-header">
                        <a href="{{url('admin/layanan/input')}}">
                            <input type="button" value="Tambah" class="btn btn-primary" name="">
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Gambar</th>
                                    <th>Nama Layanan</th>
                                    <th>Deskripsi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['data-layanan'] as $layanan)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>
                                        <img height="50px" src="{{url('')}}/{{$layanan->img_layanan}}" alt="">
                                    </td>
                                    <td>{{$layanan['name_layanan']}}</td>
                                    <td>{{$layanan['desc_layanan']}}</td>
                                    <td>
                                        
                                        <form action="{{ route('admin.layanan.delete', ['id' => $layanan -> id]) }}" method="POST">
                                            <a href="{{ route('admin.layanan.edit', ['id' => $layanan -> id]) }}" class="btn btn-primary">
                                                <span class="fa fa-pencil"></span>
                                            </a>
                                            
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" type="submit">
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </form>
                                        
                                    </td>
                                </tr>
                                
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection

@section('footer')
@include('admin-page.footer')
@endsection