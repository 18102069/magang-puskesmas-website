@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<!-- Isi File siswa.php-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Penilaian Masyarakat
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/pengaduan')}}">Penilaian Masyarakat</a></li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                
                <!-- /.box -->
                
                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Pengunjung</th>
                                    <th>Nomor Telepon</th>
                                    <th>Pesan</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach ($data['data-pengaduan'] as $pengaduan)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$pengaduan['date']}}</td>
                                    <td>{{$pengaduan['name_user']}}</td>
                                    <td>{{$pengaduan['no_user']}}</td>
                                    <td>{{$pengaduan['message_user']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
    
@section('footer')
@include('admin-page.footer')
@endsection