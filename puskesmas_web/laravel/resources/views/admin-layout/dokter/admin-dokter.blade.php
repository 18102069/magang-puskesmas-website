@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<!-- Isi File siswa.php-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dokter Puskesmas Kajen 1
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/dokter')}}">Dokter</a></li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                
                <!-- /.box -->
                
                <div class="box">
                    <div class="box-header">
                        <a href="{{url('admin/dokter/input')}}">
                            <input type="button" value="Tambah" class="btn btn-primary" name="">
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Gambar</th>
                                    <th>Nama Dokter</th>
                                    <th>Spesialis</th>
                                    <th>Jadwal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach ($data['data-dokter'] as $dokter)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>
                                        <img height="50px" src="{{url('')}}/{{$dokter->img_doctors}}" alt="">
                                    </td>
                                    <td>{{$dokter['name_doctors']}}</td>
                                    <td>{{$dokter['specialist_doctors']}}</td>
                                    <td>{{$dokter['schedule_doctors']}}</td>
                                    <td>
                                        <form action="{{ route('admin.dokter.delete', ['id' => $dokter -> id]) }}" method="POST">
                                            <a href="{{ route('admin.dokter.edit', ['id' => $dokter -> id]) }}" class="btn btn-primary">
                                                <span class="fa fa-pencil"></span>
                                            </a>
                                            
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" type="submit">
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
    
@section('footer')
@include('admin-page.footer')
@endsection