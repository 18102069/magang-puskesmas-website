@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Dokter
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/dokter')}}"> Dokter</a></li>
            <li> Edit</li>
        </ol>
    </section>
    <!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        
                        <!-- /.card-header -->
                        <div class="box">
                            
                            @if ($data['mode'] == 'Edit')
                            <form action="{{ route('admin.dokter.update', ['id' => $data['data-dokter']['id']]) }}" method="POST" enctype="multipart/form-data">
                                @method('PATCH')
                                @elseif($data['mode'] == 'Add')
                                <form action="{{ route('admin.dokter.store') }}" method="POST" enctype="multipart/form-data">
                                    @endif
                                    
                                    @csrf
                                    <div class="form-group">
                                        <label for="name_doctors">Nama Dokter</label>
                                        
                                        <input type="text" class="@error('name_doctors') is-invalid @enderror input-group" name="name_doctors" id="name_doctors" @if ($data['data-dokter'] != null) value="{{$data['data-dokter']['name_doctors']}}" @endif>
                                        
                                        @error('name_doctors')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="specialist_doctors">Spesialis</label>
                                        @error('specialist_doctors')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <textarea class="form-control @error('specialist_doctors') is-invalid @enderror" name="specialist_doctors" id="specialist_doctors" cols="30" rows="10">@if ($data['data-dokter'] != null) {{$data['data-dokter']['specialist_doctors']}} @endif</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="schedule_doctors">Jadwal (Hari - Hari)</label>
                                        @error('schedule_doctors')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <textarea class="form-control @error('schedule_doctors') is-invalid @enderror" name="schedule_doctors" id="schedule_doctors" cols="30" rows="10">@if ($data['data-dokter'] != null) {{$data['data-dokter']['schedule_doctors']}} @endif</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="img_doctors">Gambar</label>
                                        
                                        @error('img_doctors')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                        
                                        <input type="file" class="@error('img_doctors') is-invalid @enderror" name="img_doctors" id="img_doctors"></input>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-primary btn-block">Edit</button>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </div>
    @endsection
    
    @section('footer')
    @include('admin-page.footer')
    @endsection