@extends('admin-page.app')

@section('additional-stylesheet')

@endsection

@section('navbar')
@include('admin-page.navbar')
@endsection

@section('aside')
@include('admin-page.aside')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Artikel
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/artikel')}}">Artikel</a></li>
            <li>Edit</li>
        </ol>
    </section>
    <!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        
                        <!-- /.card-header -->
                        <div class="box">
                            
                            @if ($data['mode'] == 'Edit')
                            <form action="{{ route('admin.artikel.update', ['id' => $data['data-artikel']['id']]) }}" method="POST" enctype="multipart/form-data">
                                @method('PATCH')
                                @elseif($data['mode'] == 'Add')
                                <form action="{{ route('admin.artikel.store') }}" method="POST" enctype="multipart/form-data">
                                    @endif
                                    
                                    @csrf
                                    <div class="form-group">
                                        <label for="author_article">Nama Penulis</label>
                                        
                                        <input type="text" class="@error('author_article') is-invalid @enderror input-group" name="author_article" id="author_article" @if ($data['data-artikel'] != null) value="{{$data['data-artikel']['author_article']}}" @endif>
                                        
                                        @error('author_article')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="title_article">Judul artikel</label>
                                        @error('title_article')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <textarea class="form-control @error('title_article') is-invalid @enderror" name="title_article" id="title_article" cols="30" rows="10">@if ($data['data-artikel'] != null) {{$data['data-artikel']['title_article']}} @endif</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="desc_article">Deskripsi</label>
                                        @error('desc_article')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <textarea class="form-control @error('desc_article') is-invalid @enderror" name="desc_article" id="desc_article" cols="30" rows="10">@if ($data['data-artikel'] != null) {{$data['data-artikel']['desc_article']}} @endif</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="img_article">Gambar</label>
                                        
                                        @error('img_article')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                        
                                        <input type="file" class="@error('img_article') is-invalid @enderror" name="img_article" id="img_article"></input>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-primary btn-block">Edit</button>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </div>
    @endsection
    
    @section('footer')
    @include('admin-page.footer')
    @endsection