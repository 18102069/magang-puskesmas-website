@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
<link href="{{url('')}}/laravel/resources/css/information-style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">
  
  <!-- ======= Services Section ======= -->
  <section id="information" class="information">
    <div class="container">
      
      <div class="section-title" style="margin-top: 10%">
        <h2>Artikel</h2>
        
        <div class="container-info body-info" style="padding: 20px 0px">
          <div class="col-md-8">
            <div class="cp-news-list" style="padding: 10px 5px">
              @foreach ($data['data-artikel'] as $artikel)
              
              <ul class="row" style="list-style:none; padding:unset">
                <li class="col-md-5 col-sm-5">
                  <div class="">
                    <img alt="" src="{{url('')}}/{{$artikel->img_article}}" style="width:300px; height:150px">
                  </div>
                </li>
                <li class="col-md-7 col-sm-7">
                  <div class="cp-post-content">
                    <h3>
                      <a href="{{ route('artikel.showDetail', ['artikel' => $artikel->id]) }}">
                        <p style="font-size:25px; line-height:1.3">{{$artikel['title_article']}}</p>
                      </a>
                    </h3>
                    <p>
                      ditulis oleh: {{$artikel['author_article']}}
                    </p>
                    <p class="far fa-calendar-alt">
                      {{$artikel['created_at']}}
                    </p>
                  </div>
                </li>
              </ul>
              
              @endforeach
            </div>
          </div>
          
          @include('landing-page.aside')
        </div>
      </div>
    </section>
    <!-- End information Section -->
    
  </main><!-- End #main -->
  @endsection
  
  @section('chat')
  @include('landing-page.chat')
  @endsection
  
  @section('footer')
  @include('landing-page.footer')
  @endsection