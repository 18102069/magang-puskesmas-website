<!-- ======= Top Bar ======= -->
<div id="topbar" class="d-flex align-items-center fixed-top">
    <div class="container d-flex justify-content-between">
        <div class="contact-info d-flex align-items-center">
            <i class="bi bi-envelope"></i> <a href="mailto:contact@example.com">contact@example.com</a>
            <i class="bi bi-phone"></i> (0285) 381781
        </div>
    </div>
</div>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
        
        <h1 class="logo me-auto"><a href="{{url('')}}">
            Puskesmas Kajen 1
        </a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        
        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a class="nav-link 
                    @if ($data['active-page'] == 'home')
                    active
                    @endif " href="{{url('')}}">Beranda</a></li>
                    
                    <li class="dropdown"><a class="nav-link 
                        @if ($data['active-page'] == 'profil')
                        active
                        @endif " href="{{url('profil')}}"><span>Profil</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a class="nav-link 
                                @if ($data['active-page'] == 'profil')
                                active
                                @endif " href="{{url('profil')}}">Profil Puskesmas Kajen 1</a></li>
                                <li><a class="nav-link 
                                    @if ($data['active-page'] == 'profil')
                                    active
                                    @endif " href="{{url('profil/denah')}}">Denah Puskesmas Kajen 1</a></li>
                                    <li><a class="nav-link 
                                        @if ($data['active-page'] == 'profil')
                                        active
                                        @endif " href="{{url('profil/visimisi')}}">Visi dan Misi</a></li>
                                    </ul>
                                </li>
                                
                                <li><a class="nav-link 
                                    @if ($data['active-page'] == 'layanan')
                                    active
                                    @endif " href="{{url('layanan')}}">Layanan</a>
                                </li>
                                
                                <li class="dropdown"><a class="nav-link 
                                    @if ($data['active-page'] == 'informasi')
                                    active
                                    @endif " href="{{url('informasi')}}"><span>Informasi</span> <i class="bi bi-chevron-down"></i></a>
                                    <ul>
                                        <li><a class="nav-link 
                                            @if ($data['active-page'] == 'informasi')
                                            active
                                            @endif " href="{{url('berita')}}">Berita</a>
                                        </li>
                                        
                                        <li><a class="nav-link 
                                            @if ($data['active-page'] == 'informasi')
                                            active
                                            @endif " href="{{url('artikel')}}">Artikel</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="nav-link
                                    @if ($data['active-page'] == 'dokter')
                                    active
                                    @endif " href="{{url('dokter')}}">Dokter</a></li>
                                <li><a class="nav-link
                                    @if ($data['active-page'] == 'kontak')
                                    active
                                    @endif " href="{{url('kontak')}}">Kontak</a></li>
                                <li><a class="nav-link" href="{{url('login')}}">Masuk</a></li>
                            </ul>
                            <i class="bi bi-list mobile-nav-toggle"></i>
                        </nav><!-- .navbar -->
                        
                    </div>
                </header>
                <!-- End Header -->