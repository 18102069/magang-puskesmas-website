<!DOCTYPE html>
<html lang="en">
  <!-- =======================================================
  * Template Name: Medilab - v4.3.0
  * Template URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Puskesmas Kajen 1</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{url('')}}/laravel/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="{{url('')}}/laravel/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{url('')}}/laravel/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{url('')}}/laravel/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{url('')}}/laravel/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="{{url('')}}/laravel/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{url('')}}/laravel/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{url('')}}/laravel/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Additional CSS File -->
  @yield('additional-stylesheet')

</head>

<body>

    @yield('navbar')

    @yield('content')

    @yield('chat')

    @yield('footer')

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{url('')}}/laravel/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{url('')}}/laravel/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{url('')}}/laravel/vendor/php-email-form/validate.js"></script>
  <script src="{{url('')}}/laravel/vendor/purecounter/purecounter.js"></script>
  <script src="{{url('')}}/laravel/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="{{url('')}}/laravel/resources/js/main.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</body>

</html>