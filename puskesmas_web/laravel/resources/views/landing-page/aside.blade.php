<div class="col-md-4 col-xs-12 padtop">
    <div class="col-md-12 col-xs-12 no-pad">
      <ul class="nav-info nav-info-tabs mama" style="background-color: unset;">
        <li class="active li-info"><a href="#" data-toggle="tab">Berita Terkini</a></li>
      </ul>
      
      <div class="col-md-12" style="padding: unset; margin-top: 20px">
        <div class="cp-news-list">

          {{-- {{  var_dump($data['data-berita'][0]) }} --}}

          @foreach ($data['berita-aside'] as $berita)

          <ul class="col-xs-12" style="list-style: none; padding:0px">
            <li class="col-md-4 col-xs-4" style="padding:0px">
              <div class="">
                <img src="{{url('')}}/{{$berita->img_news}}" style="width:100%">
              </div>
            </li>
            <li class="col-md-8 col-xs-8">
              <div class="cp-post-content">
                <span style="font-size:13px"><i class="far fa-calendar-alt" aria-hidden="true"></i> {{$berita['created_at']}}</span>
                <a href="{{ route('berita.showDetail', ['berita' => $berita->id]) }}">
                    <p style="font-size:12px; line-height:unset">
                        <b>{{$berita['title_news']}}</b>
                    </p>
                </a>
              </div>
            </li>
          </ul>

          @endforeach

        </div>
      </div>

      <ul class="nav-info nav-info-tabs mama" style="background-color: unset;">
        <li class="active li-info"><a style="padding: 10px 6px; margin-top: 20px" href="#" data-toggle="tab">Artikel</a></li>
      </ul>

      <div class="col-md-12" style="padding: unset; margin-top: 20px">
        <div class="cp-news-list">

          @foreach ($data['artikel-aside'] as $artikel)

          <ul class="col-xs-12" style="list-style: none; padding:0px">
            <li class="col-md-4 col-xs-4" style="padding:0px">
              <div class="">
                <img src="{{url('')}}/{{$artikel->img_article}}" style="width:100%">
              </div>
            </li>
            <li class="col-md-8 col-xs-8">
              <div class="cp-post-content">
                <span style="font-size:13px"><i class="far fa-calendar-alt" aria-hidden="true"></i> {{$artikel['created_at']}}</span>
                <a href="{{ route('artikel.showDetail', ['artikel' => $artikel->id]) }}">
                    <p style="font-size:12px; line-height:unset">
                        <b>{{$artikel['title_article']}}</b>
                    </p>
                </a>
              </div>
            </li>
          </ul>

          @endforeach
          
        </div>
      </div>
    </div>
  </div>