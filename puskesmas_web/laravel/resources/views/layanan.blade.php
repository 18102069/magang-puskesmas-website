@extends('landing-page.app')

@section('additional-stylesheet')
  <link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
@endsection

@section('navbar')
  @include('landing-page.navbar')
    
@endsection

@section('content')
<main id="main" style="margin-top: 10px">

  <!-- ======= Services Section ======= -->
  <section id="services" class="services">
    <div class="container">

      <div class="section-title" style="margin-top: 10%">
        <h2>Layanan</h2>
        
      </div>

      <div class="row">

        @foreach ($data['data-layanan'] as $layanan)

        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
          <div class="icon-box">
            <div>
              <img height="150px" src="{{url('')}}/{{$layanan->img_layanan}}" alt="">
            </div>
            <h4 style="margin-top: 20px">{{$layanan['name_layanan']}}</h4>
            <p>{{$layanan['desc_layanan']}}</p>
          </div>
        </div>

        @endforeach

      </div>

    </div>
  </section><!-- End Services Section -->

</main><!-- End #main -->
@endsection

@section('chat')
    @include('landing-page.chat')
@endsection

@section('footer')
    @include('landing-page.footer')
@endsection
