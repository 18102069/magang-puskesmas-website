<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Login</title>

  <!-- Template Main CSS File -->
  <link href="{{url('')}}/laravel/resources/css/login-style.css" rel="stylesheet">
  
</head>
<!-- <body class="login-form"> -->
  <body>
  <!-- Main -->
  <section class="bg-image">
    <div class="d-flex flex-column flex-root bg-content">
      <!-- Login -->
      <div class="login login-3 login-signin-on d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid">
          <div class="login-form text-center text-white p-7 position-relative overflow-hidden">   
            <!--begin::Login Sign in form-->
            <div class="login-signin">
              <div class="mb-20">
                <h2>Puskesmas Admin</h2>
              </div>

              @if (session()->has('pesan'))
              <div class="alert alert-success">
                {{ session()->get('pesan') }}
              </div>
              @endif
  
              <form class="form" action="{{ route('login.process') }}" method="post">
                @csrf
                <div class="form-group">
                  <input class="form-control h-auto text-white placeholder-white opacity-70 bg-white-o-30 rounded-pill border-0 py-4 px-8 mb-5 @error('username') is-invalid @enderror"" type="text" placeholder="Username" name="username" value=""{{ old('username') }}">
                  
                  @error('username')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror

                </div>

                <div class="form-group">
                  <input class="form-control h-auto text-white placeholder-white opacity-70 bg-white-o-30 rounded-pill border-0 py-4 px-8 mb-5 @error('password') is-invalid @enderror" type="password" placeholder="Password" name="password" value="{{ old('password') }}">

                  @error('password')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror

                </div>
                
                <div class="form-group text-center mt-10">
                  <button type="submit" class="btn btn-pill btn-outline-white font-weight-bold opacity-90 px-15 py-3">Masuk</button>
                </div>
              </form>
            </div>
            <!--end::Login Sign in form-->
      
          </div>
        </div>
      </div>
    </div>
  </section>

</body>
</html>
