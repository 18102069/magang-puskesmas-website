@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
<link href="{{url('')}}/laravel/resources/css/information-style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<!-- ======= Kontak Section ======= -->
<section id="contact" class="contact">
  <div class="container">
    
    <div class="section-title" style="margin-top: 10%">
      <h2>Kontak Kami</h2>
    </div>
  </div>
  
  <div>
    <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.8729009009694!2d109.58877891434855!3d-7.02422277076312!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e701fed19ca62bf%3A0xf230092811ee034!2sDinas%20Komunikasi%20Dan%20Informatika%20Kabupaten%20Pekalongan!5e0!3m2!1sen!2sid!4v1630245135482!5m2!1sen!2sid" frameborder="0" allowfullscreen></iframe>
  </div>
  
  <div class="container">
    <div class="row mt-5">
      
      <div>
        <div class="info">
          <div class="address">
            <i class="bi bi-geo-alt"></i>
            <h4>Lokasi:</h4>
            <p>Jl. Krakatau No.2, Tambor, Nyamok, Kec. Kajen, Pekalongan, Jawa Tengah 51161</p>
          </div>
          
          <div class="email">
            <i class="bi bi-envelope"></i>
            <h4>Website:</h4>
            <p>https://dinkominfo.pekalongankab.go.id/</p>
          </div>
          
          <div class="phone">
            <i class="bi bi-phone"></i>
            <h4>Telepon:</h4>
            <p>(0285) 381781</p>
          </div>
          
        </div>
        
      </div>
      
    </div>
    
  </div>
</section><!-- End Contact Section -->
@endsection

@section('chat')
@include('landing-page.chat')
@endsection

@section('footer')
@include('landing-page.footer')
@endsection
