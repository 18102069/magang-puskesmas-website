<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('')}}/laravel/resources/assets/img/admin/admin-profil.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="
          @if ($data['active'] == 'admin-dashboard')
              active
          @endif">
          <a href="{{url('admin/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li class="
          @if ($data['active'] == 'admin-layanan')
              active
          @endif">
          <a href="{{url('admin/layanan')}}">
            <i class="fa fa-files-o"></i> <span>Layanan</span>  
          </a>  
        </li>

        <li class="
          @if ($data['active'] == 'admin-dokter')
              active
          @endif">
          <a href="{{url('admin/dokter')}}">
            <i class="fa fa-stethoscope"></i> <span>Dokter</span>
          </a>
        </li>

        <li class="
          @if ($data['active'] == 'admin-berita')
              active
          @endif">
          <a href="{{url('admin/berita')}}">
            <i class="fa fa-pie-chart"></i><span>Berita</span>
          </a>
        </li>

        <li class="
          @if ($data['active'] == 'admin-artikel')
              active
          @endif">
          <a href="{{url('admin/artikel')}}">
            <i class="fa fa-laptop"></i><span>Artikel</span>
          </a>
        </li>

        <li class="
          @if ($data['active'] == 'admin-galeri')
              active
          @endif ">
          <a href="{{url('admin/galeri')}}">
            <i class="fa fa-book"></i> <span>Galeri</span>
          </a>
        </li>

        <li class="
          @if ($data['active'] == 'admin-pengaduan')
              active
          @endif ">
          <a href="{{url('admin/pengaduan')}}">
            <i class="fa fa-comments"></i> <span>Penilaian Masyarakat</span>
          </a>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->