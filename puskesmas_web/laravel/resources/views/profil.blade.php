@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">
  
  <!-- ======= Profil/About Section ======= -->
  <section id="about" class="about">
    <div class="container">
      <div class="section-title" style="margin-top: 10%">
        <h2>Profil Puskesmas Kajen 1</h2>
        <img class="img-profil" src="{{url('')}}/laravel/resources/assets/img/gedung_dinkominfo.jpeg" alt="">
        <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>
      
    </div>
  </section>
  
</main><!-- End #main -->
@endsection

@section('chat')
@include('landing-page.chat')
@endsection

@section('footer')
@include('landing-page.footer')
@endsection
