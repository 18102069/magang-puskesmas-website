@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
<link href="{{url('')}}/laravel/resources/css/information-style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">
  
  <!-- ======= Services Section ======= -->
  <section id="information" class="information">
    <div class="container">
      
      <div class="section-title" style="margin-top: 10%">
        <h2>Informasi Terkini</h2>
        
        <div class="container-info body-info" style="padding: 20px 0px">
          <div class="col-md-8">
            <p class="p-info"></p>
            <p>-</p>
            <p></p>
          </div>
          
          @include('landing-page.aside')
        </div>
      </div>
    </section>
    <!-- End information Section -->
    
  </main><!-- End #main -->
  @endsection
  
  @section('chat')
  @include('landing-page.chat')
  @endsection
  
  @section('footer')
  @include('landing-page.footer')
  @endsection