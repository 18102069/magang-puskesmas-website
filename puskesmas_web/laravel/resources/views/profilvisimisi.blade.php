@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">

  <!-- ======= Profil/About Section ======= -->
  <section id="about" class="about">
    <div class="container">
      <div class="section-title" style="margin-top: 10%">
        <h2>Visi dan Misi</h2>

        <div class="row" style="padding: 30px">
          <div class="icon-box">
            <h4 class="pro"><strong>Visi</strong></h4>
            <p class="des">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
          </div>

          <div class="icon-box">
            <h4 class="pro"><strong>Misi</strong></h4>
            <p class="des">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
          </div>

        </div>

        <div id="services" class=" services row" style="padding: 30px">
            <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div class="icon-box">
                <h4><strong>Peran</strong></h4>
                <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
              </div>
            </div>
  
            <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
              <div class="icon-box">
                <h4><strong>Fungsi</strong></h4>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
              </div>
            </div>
  
            <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
              <div class="icon-box">
                <h4><strong>Tugas</strong></h4>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
              </div>
            </div>
  
          </div>
        
      </div>

    </div>
  </section>

</main><!-- End #main -->
@endsection

@section('chat')
@include('landing-page.chat')
@endsection

@section('footer')
@include('landing-page.footer')
@endsection
