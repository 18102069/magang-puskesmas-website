@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
<link href="{{url('')}}/laravel/resources/css/testimoni-style.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="js/modernizr.js"></script>
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center" style="background-image: url('{{url('')}}/laravel/resources/assets/img/landing-page.jpg');">
  <div class="container">
    <h1>Selamat Datang!</h1>
    <h2>Sehat Bersama Puskesmas</h2>
  </div>
</section><!-- End Hero -->

<main id="main" style="margin-top: 10px">
  
  <!-- ======= Profil/About Section ======= -->
  <section id="about" class="about">
    <div class="container">
      <div class="section-title">
        <h2>Puskesmas</h2>
        <p style="font-size: large">
          Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.
        </p>
      </div>
      
    </div>
  </section>
  
  <!-- ======= Departments Section ======= -->
  <section id="departments" class="departments">
    <div class="container">
      
      <div class="section-title">
        <h2>Layanan</h2>
        <p style="font-size: large">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
      </div>
      
      <div class="row">
        <div class="col-lg-3">
          <ul class="nav nav-tabs flex-column">
            
            @foreach ($data['data-layanan'] as $layanan)
            
            <li class="nav-item">
              <a class="nav-link @if ($loop->iteration == 1) 
                active show
                @endif" data-bs-toggle="tab" href="#tab-{{$loop->iteration}}" style="font-size: medium;">{{$layanan['name_layanan']}}
              </a>
            </li>
            
            @endforeach
            
          </ul>
        </div>
        <div class="col-lg-9 mt-4 mt-lg-0">
          <div class="tab-content ">
            
            @foreach ($data['data-layanan'] as $layanan)
            
            <div class="tab-pane @if ($loop->iteration == 1) 
              active
              @endif" id="tab-{{$loop->iteration}}">
              <div class="row">
                <div class="col-lg-8 details order-2 order-lg-1">
                  <h3>{{$layanan['name_layanan']}}</h3>
                  <p style="font-size: medium">{{$layanan['desc_layanan']}}</p>
                </div>
                <div class="col-lg-4 text-center order-1 order-lg-2">
                  <img src="{{url('')}}/{{$layanan->img_layanan}}" alt="" class="img-fluid">
                </div>
              </div>
            </div>
            
            @endforeach
            
          </div>
        </div>
      </div>
      
    </div>
  </section><!-- End Departments Section -->
  
  <!-- ======= Dokter Section ======= -->
  <section id="doctors" class="doctors">
    <div class="container">
      
      <div class="section-title">
        <h2>Dokter</h2>
      </div>
      
      <div class="row">
        
        @foreach ($data['data-dokter'] as $dokter)
        
        <div class="col-lg-6">
          <div class="member d-flex align-items-start">
            
            <div class="pic"><img src="{{url('')}}/{{$dokter->img_doctors}}" class="img-fluid" alt=""></div>
            
            <div class="member-info">
              <h4 style="font-family: Raleway, sans-serif;">{{$dokter['name_doctors']}}</h4>
              <span>{{$dokter['specialist_doctors']}}</span>
              <p style="font-size: medium">Jadwal Dokter: </p>
              <p style="font-size: medium">{{$dokter['schedule_doctors']}}</p>
              
            </div>
          </div>
        </div>
        
        @endforeach
        
      </div>
      
    </div>
  </section>
  <!-- End Doctors Section -->
  
  <!-- ======= Frequently Asked Questions Section ======= -->
  <section id="faq" class="faq section-bg">
    <div class="container">
      
      <div class="section-title">
        <h2>Pertanyaan yang Sering Diajukan</h2>
      </div>
      
      <div class="faq-list">
        <ul>
          <li data-aos="fade-up">
            <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" class="collapse" data-bs-target="#faq-list-1">Non consectetur a erat nam at lectus urna duis? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
            <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
              <p>
                Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
              </p>
            </div>
          </li>
          
          <li data-aos="fade-up" data-aos-delay="100">
            <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-2" class="collapsed">Feugiat scelerisque varius morbi enim nunc? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
            <div id="faq-list-2" class="collapse" data-bs-parent=".faq-list">
              <p>
                Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
              </p>
            </div>
          </li>
          
          <li data-aos="fade-up" data-aos-delay="200">
            <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-3" class="collapsed">Dolor sit amet consectetur adipiscing elit? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
            <div id="faq-list-3" class="collapse" data-bs-parent=".faq-list">
              <p>
                Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
              </p>
            </div>
          </li>
          
          <li data-aos="fade-up" data-aos-delay="300">
            <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-4" class="collapsed">Tempus quam pellentesque nec nam aliquam sem et tortor consequat? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
            <div id="faq-list-4" class="collapse" data-bs-parent=".faq-list">
              <p>
                Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in.
              </p>
            </div>
          </li>
          
          <li data-aos="fade-up" data-aos-delay="400">
            <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-5" class="collapsed">Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
            <div id="faq-list-5" class="collapse" data-bs-parent=".faq-list">
              <p>
                Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque.
              </p>
            </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section><!-- End Frequently Asked Questions Section -->
  
  <!-- ======= Gallery Section ======= -->
  <section id="gallery" class="gallery">
    <div class="container">
      
      <div class="section-title">
        <h2>Galeri</h2>
        
      </div>
    </div>
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      
      <!-- Indicators -->
      <ol class="carousel-indicators">
        @foreach ($data['data-galeri'] as $galeri)
        <li data-target="#myCarousel" data-slide-to="{{$loop->iteration}}" class="@if ($loop->iteration == 1)
          active
          @endif">
        </li>
        @endforeach
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        @foreach ($data['data-galeri'] as $galeri)
        
        <div class="item @if ($loop->iteration == 1)
          active
          @endif">
          <img src="{{url('')}}/{{$galeri->img_galery}}" alt="{{$galeri['title_galery']}}" style="align-content: center; width:50%;">
          <div class="carousel-caption d-none d-md-block">
            <h3>
              {{$galeri['title_galery']}}
            </h3>
          </div>
        </div>
        
        @endforeach
        
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      
    </div>
    
  </section><!-- End Gallery Section -->
  
  <!-- ======= Penilaian Section ======= -->
  <section id="appointment" class="appointment section-bg">
    <div class="container">
      
      <div class="section-title">
        <h2>Penilaian Masyarakat</h2>
        <p style="font-size: medium">Fitur pengaduan ini digunakan dalam meningkatkan kualitas dari kinerja Puskesmas Kajen 1</p>
      </div>
      
      <form action="{{ route('pengaduan.store') }}" method="post">
        
        @csrf
        <div class="row form-group">
          
          <div class="col-md-4 form-group">
            <label for="name_user"></label>
            <input type="text" class="form-control @error('name_user') is-invalid @enderror" name="name_user" id="name_user" @if ($data['data-pengaduan'] != null) value="{{$data['data-pengaduan']['name_user']}}" @endif placeholder="Nama Pengunjung" data-rule="minlen:4" data-msg="Tolong isi setidaknya 4 karakter">
            
            @error('name_user')
            <div class="text-danger">
              {{$message}}
            </div>
            @enderror
          </div>
          
          <div class="col-md-4 form-group mt-3 mt-md-0">
            <label for="no_user"></label>
            <input type="text" class="form-control @error('no_user') is-invalid @enderror" name="no_user" id="no_user" @if ($data['data-pengaduan'] != null) value="{{$data['data-pengaduan']['no_user']}}" @endif placeholder="Nomor Telepon" data-rule="minlen:4" data-msg="Tolong isi setidaknya 4 karakter">
            
            @error('no_user')
            <div class="text-danger">
              {{$message}}
            </div>
            @enderror
          </div>
          
          <div class="col-md-4 form-group mt-3 mt-md-0">
            <label for="date"></label>
            <input type="date" class="form-control datepicker @error('date') is-invalid @enderror" name="date" id="date" @if ($data['data-pengaduan'] != null) value="{{$data['data-pengaduan']['date']}}" @endif placeholder="Tanggal Pengaduan" data-rule="minlen:4">
            
            @error('date')
            <div class="text-danger">
              {{$message}}
            </div>
            @enderror
          </div>
        </div>
        
        <div class="form-group mt-3">
          <label for="message_user"></label>
          <textarea class="form-control @error('message_user') is-invalid @enderror" name="message_user" id="message_user" @if ($data['data-pengaduan'] != null) value="{{$data['data-pengaduan']['message_user']}}" @endif rows="5" placeholder="Saran dan Kritik"></textarea>
          
          @error('message_user')
          <div class="text-danger">
            {{$message}}
          </div>
          @enderror
        </div>
        
        <div class="text-center">
          <button type="submit" class="btn btn-primary">Kirim</button>
        </div>
        
      </form>
      
    </div>
  </section><!-- End Appointment Section -->
  
  <!-- ======= Highlihgt Penilaian Section ======= -->
  <section id="testimonials" class="testimonials section-bg">
    
    <div class="container">
      
      <div class="section-title">
        <h2>Apa Kata Masyrakat</h2>
      </div>  	
      
      <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
        <div class="swiper-wrapper">
          
          @foreach ($data['pengaduan-highlight'] as $pengaduan)
          
          <div class="swiper-slide">
            <div class="testimonial-item">
              
              <h3>{{$pengaduan['name_user']}}</h3>
              
              <span>
                {{$pengaduan['created_at']}}
              </span>
              
              <p style="margin: 20px; font-size: large;">
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                {{$pengaduan['message_user']}}
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              
              
              {{-- <h4>Ceo &amp; Founder</h4> --}}
            </div>
          </div><!-- End testimonial item -->
          
          @endforeach
          
        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div>
    
  </section> <!-- /testimonials -->
  
  {{-- <section id="pengaduan" class="pengaduan">
    
    <div class="container">
      <div class="section-title">
        <h2>Pengaduan Masyarakat</h2>
      </div>
      
      <div id="disqus_thread">

      </div>
      <script>
        /**
        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT 
        *  THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR 
        *  PLATFORM OR CMS.
        *  
        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: 
        *  https://disqus.com/admin/universalcode/#configuration-variables
        */
        /*
        var disqus_config = function () {
          // Replace PAGE_URL with your page's canonical URL variable
          this.page.url = PAGE_URL;  
          
          // Replace PAGE_IDENTIFIER with your page's unique identifier variable
          this.page.identifier = PAGE_IDENTIFIER; 
        };
        */
        
        (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
          var d = document, s = d.createElement('script');
          
          // IMPORTANT: Replace EXAMPLE with your forum shortname!
          s.src = 'https://EXAMPLE.disqus.com/embed.js';

          // s.src = 'https://puskesmas-kajen-1.disqus.com/embed.js';
          
          s.setAttribute('data-timestamp', +new Date());
          (d.head || d.body).appendChild(s);
        })();
      </script>
      
      <noscript>
        Please enable JavaScript to view the 
        <a href="https://disqus.com/?ref_noscript" rel="nofollow">
          comments powered by Disqus.
        </a>
      </noscript>
      
    </div>
    
  </section> --}}
  
  <!-- ======= Kontak Section ======= -->
  <section id="contact" class="contact">
    <div class="container">
      
      <div class="section-title">
        <h2>Kontak Kami</h2>
      </div>
    </div>
    
    <div>
      <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.8729009009694!2d109.58877891434855!3d-7.02422277076312!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e701fed19ca62bf%3A0xf230092811ee034!2sDinas%20Komunikasi%20Dan%20Informatika%20Kabupaten%20Pekalongan!5e0!3m2!1sen!2sid!4v1630245135482!5m2!1sen!2sid" frameborder="0" allowfullscreen></iframe>
    </div>
    
    <div class="container">
      <div class="row mt-5">
        
        <div>
          <div class="info">
            <div class="address">
              <i class="bi bi-geo-alt"></i>
              <h4>Lokasi:</h4>
              <p style="font-size: medium">Jl. Krakatau No.2, Tambor, Nyamok, Kec. Kajen, Pekalongan, Jawa Tengah 51161</p>
            </div>
            
            <div class="email">
              <i class="bi bi-envelope"></i>
              <h4>Website:</h4>
              <p style="font-size: medium">https://dinkominfo.pekalongankab.go.id/</p>
            </div>
            
            <div class="phone">
              <i class="bi bi-phone"></i>
              <h4>Telepon:</h4>
              <p style="font-size: medium">(0285) 381781</p>
            </div>
            
          </div>
          
        </div>
        
      </div>
      
    </div>
  </section><!-- End Contact Section -->
  
</main><!-- End #main -->
@endsection

@section('chat')
@include('landing-page.chat')
@endsection

@section('footer')
@include('landing-page.footer')
@endsection