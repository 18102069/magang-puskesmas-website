@extends('landing-page.app')

@section('additional-stylesheet')
<link href="{{url('')}}/laravel/resources/css/style.css" rel="stylesheet">
<link href="{{url('')}}/laravel/resources/css/information-style.css" rel="stylesheet">
@endsection

@section('navbar')
@include('landing-page.navbar')

@endsection

@section('content')
<main id="main" style="margin-top: 10px">
  
  <!-- ======= Services Section ======= -->
  <section id="information" class="information">
    <div class="container">
      
      <div class="section-title" style="margin-top: 10%">
        <h2>{{$data['berita-id']->title_news}}</h2>
        
        <div class="container-info body-info" style="padding: 20px 0px">
          <div class="col-md-8">

            <div style="text-align: left">
              <p class="far fa-calendar-alt">
                <strong>
                  {{$data['berita-id']->created_at}}
                </strong>
              </p>

              <p class="fas fa-user">
                Ditulis Oleh : 
                <strong>
                  {{$data['berita-id']->author_news}}
                </strong>
              </p>
            </div>
            
            {{-- <ul class="list-inline font-weight-600 myriadpro">
              <i class="fa fa-calendar">
                {{$data['berita-id']->created_at}}
              </i>
              
              <i class="fa fa-user">
                "Ditulis Oleh : "
                <span class="label label-default">
                  {{$data['berita-id']->author_news}}
                </span>
              </i>
            </ul> --}}
            
            <div>
              <img src="{{url('')}}/{{$data['berita-id']->img_news}}" alt="" style="align-items: center; width: 600px; margin-top: 20px;">
            </div>
            
            <div>
              <h3 style="text-align: center; margin-top: 20px;">
                <strong>
                  {{$data['berita-id']->title_news}}
                </strong>
              </h3>
              <p style="font-size: medium; text-align: justify; margin-top: 20px;">
                {{$data['berita-id']->desc_news}}
              </p>
            </div>
          </div>
          
          @include('landing-page.aside')
        </div>
      </div>
    </section>
    <!-- End information Section -->
    
  </main><!-- End #main -->
  @endsection
  
  @section('chat')
  @include('landing-page.chat')
  @endsection
  
  @section('footer')
  @include('landing-page.footer')
  @endsection